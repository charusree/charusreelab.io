---
layout: page
title: About
permalink: /about/
---

Life and other things from Meow's perspective.

He is a pampered house cat, masquerading as an alley cat
